- [1. Introducción](#1-introducción)
- [2. Integrantes Responsables](#2-integrantes-responsables)
- [3. Vínculos de origen](#3-vínculos-de-origen)
  - [3.1. Energía](#31-energía)
    - [3.1.1. Petróleo](#311-petróleo)
  - [3.2. Clima](#32-clima)
  - [3.3. Transporte](#33-transporte)
  - [3.4. Bienes Raices](#34-bienes-raices)
  - [3.5. Biodiversidad](#35-biodiversidad)  

# 1. Introducción

Aquí se detallan ciertos datasets que pueden ser relacionados con datos geográficos. Se incluyen los vínculos (URL) de origen así como un link de descarga directa desde este repositorio catalogados por tema. En la medida que vayamos analizando la información iremos agregando la metadata.

# 2. Integrantes Responsables

- Juan Vincens
- Agustín Genoves

# 3. Vínculos de origen

## 3.1. Energía

### 3.1.1. Petróleo  

[<span style="font-size:medium;">Listado de pozos petroleros</span>](https://www.datos.gob.ar/dataset/energia-produccion-petroleo-gas-por-pozo-capitulo-iv/archivo/energia_cbfa4d79-ffb3-4096-bab5-eb0dde9a8385) [(**Descargar**)](#)  
[<span style="font-size:medium;">Producción de petróleo y gas por pozo</span>](https://www.datos.gob.ar/dataset/energia-produccion-petroleo-gas-por-pozo-capitulo-iv/archivo/energia_cb5c0f04-7835-45cd-b982-3e25ca7d7751) [(**Descargar**)](#)  
[<span style="font-size:medium;">Precios vigentes en surtidor</span>](https://www.datos.gob.ar/dataset/energia-precios-surtidor---resolucion-3142016/archivo/energia_80ac25de-a44a-4445-9215-090cf55cfda5) [(**Descargar**)](#)  


## 3.2. Clima

[<span style="font-size:medium;">Resúmenes diarios de indicadores de precipitación</span>](https://data.humdata.org/dataset/daily-summaries-of-precipitation-indicators-for-argentina) [(**Descarga**)](#)  

## 3.3. Transporte

[<span style="font-size:medium;">Aeropuertos en Argentina</span>](https://data.humdata.org/dataset/ourairports-arg) [(**Descarga**)](#)  

## 3.4. Bienes Raices

[<span style="font-size:medium;">Alquileres y ventas</span>](https://data.world/properati/real-estate-listings-argentina) [(**Descarga Alquileres**)](#) [(**Descarga Ventas**)](#)  

## 3.5. Biodiversidad  

[<span style="font-size:medium;">Flora y Fauna Argentina</span>](https://www.gbif.org/) [(**Descarga**)](datasets/biologia/flora_y_fauna.zip)  
[<span style="font-size:medium;">Felinos de Argentina</span>](https://www.gbif.org/) [(**Descarga**)](datasets/biologia/felinos.zip)  
