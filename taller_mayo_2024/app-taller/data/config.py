import json
from pathlib import Path
import pandas as pd

DEFAULT_CONFIG = {
    "groups": [],
}


def taller_dir():
    return Path(__file__).resolve().parent.parent


def config_dir():
    return taller_dir() / "config"

def file_dir():
    return taller_dir() / "files"

def config_file():
    return config_dir() / "config.json"


def load_config():
    try:
        with config_file().open() as f:
            return json.load(f)
    except FileNotFoundError:
        return DEFAULT_CONFIG

def save_config(config):
    with config_file().open("w") as f:
        json.dump(config, f)

def read_dataset(file_df):
    try:
        with open(file_dir()/file_df) as f:
            return pd.read_csv(f)
    except FileNotFoundError:
        return 'error'
    
