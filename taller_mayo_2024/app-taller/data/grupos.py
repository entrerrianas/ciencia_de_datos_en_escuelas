from data.config import load_config, save_config

def groups_data():
    config = load_config()
    return config.get("groups", [])

def groups():
    return [u["group"] for u in groups_data()]


def validate_group(group):
    
    if group in groups():
        return False, "El grupo ya existe"
    return True, None
