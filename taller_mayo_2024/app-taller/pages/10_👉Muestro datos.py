import streamlit as st
from data.config import read_dataset
import random
import matplotlib.pyplot as plt
import folium
from streamlit_folium import folium_static, st_folium
from random import shuffle

st.title("¿Cómo podemos mostrar nuestros datos?")
aerp = read_dataset('ar-airports.csv')
tab1,tab2,tab3,tab4, tab5= st.tabs(['Recordemos', '✅ Ejercicio 3', 'Mapas',
                              '✅ Ejercicio 4', ' 🚨 Desafío' ])
with tab1:
    with st.expander("Mostrar una lista con la cantidad de cada tipo de aeropuerto"):
        code = '''aeropuertos.type.value_counts()'''
        st.code(code, language='python')
    with st.expander("Hacer un gráfico de torta"):
        code = '''conteo_tipos.plot(kind='pie', ax=ax, color='skyblue')'''
        
        st.code(code, language='python')
    with st.expander("Hacer un gráfico de barra"):
        code = '''conteo_tipos.plot(kind='bar', ax=ax, color='skyblue')'''
        
        st.code(code, language='python')
with tab2:
    st.subheader("¿Qué forma es más clara en cada caso? ")
    
    with st.expander("Quiero mostrar porporcionalmente la cantidad de cada tipo de aeropuerto"):
        fig,ax = plt.subplots()
        labels = aerp['type'].unique()
        # Crear el gráfico de barras
        conteo_tipos = aerp['type'].value_counts()
        #conteo_tipos.plot(kind='pie', ax=ax, color='skyblue')
        # Agregar etiquetas y título
        wedges,  autotexts = ax.pie(conteo_tipos,  startangle=140)

        # Configurar la leyenda
        ax.legend(wedges, labels, title="Categorías", loc="center left", bbox_to_anchor=(1, 0, 0.5, 1))

        #    Ajustar el gráfico para que la leyenda no se superponga
        plt.subplots_adjust(left=0.1, right=0.7)
        
        ax.set_title('Tipos de aeropuertos')
        # Mostrar el gráfico
        st.pyplot(fig)
    with st.expander("Quiero mostrar valores estimados de la cantidad de cada tipo de aeropuertos"):
        fig,ax = plt.subplots()
        # Crear el gráfico de barras
        conteo_tipos = aerp['type'].value_counts()
        conteo_tipos.plot(kind='bar', ax=ax, color='skyblue')
        # Agregar etiquetas y título
        ax.set_xlabel('Tipos de aeropuertos')
        ax.set_ylabel('Cantidad')
        ax.set_title('Cantidad de Aeropuertos por Tipo')
        # Mostrar el gráfico
        st.pyplot(fig)
    with st.expander("Quiero mostrar las cantidades específicas de cada tipo de aeropuertos"):
        st.write(aerp.type.value_counts())
    with st.expander("¿Y si quiero que se vea rápidamente en forma visual dónde están los aeropuertos?"):
        st.write("Usamos mapas")
    
def generar_mapa():
    attr = (
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> '
        'contributors, &copy; <a href="https://cartodb.com/attributions">CartoDB</a>'
    )
    
    tiles = 'https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/capabaseargenmap@EPSG%3A3857@png/{z}/{x}/{-y}.png'
    m = folium.Map(
        location=(-33.457606, -65.346857),
        control_scale=True,
        zoom_start=5,
        name='es',
        tiles=tiles,
        attr=attr
    )
    return m

        

with tab3:
    with st.expander("Importamos la librería folium para hacer mapas"):
        code = '''import folium''' 
        st.code(code, language='python')
    with st.expander("Generar un mapa base para luego agregar los puntos"):
        st.write("Definimos la función .....")
    with st.expander("Generamos la variable que contiene nuestro mapa base"):
        code = '''mapa = generar_mapa()''' 
        st.code(code, language='python')
    with st.expander("¿En qué columnas están los datos necesarios para ubicar geográficamente cada aeropuerto y para mostrar el nombre del aeropuerto?"):
        code = '''aeropuertos.columns''' 
        st.code(code, language='python')
        st.write(aerp.columns)
    with st.expander("Definimos la función para poder agregar los puntos"):
        code = '''def agregar_puntos_aeropuerto(row):
                    folium.Marker(
                    [row['lat'], row['lng']],
                    popup=row['name'],
                    icon=folium.Icon()
                    ).add_to(mapa)'''
        st.code(code, language='python')
    with st.expander("Agregamos los puntos al mapa"):
        code = '''aeropuertos.apply(agregar_puntos_aeropuerto, axis=1)'''
        st.code(code, language='python')
  
with tab4:
    def agregar_marca_aerop2(row):
        folium.Marker(
            [row['lat'], row['lng']],
            popup=row['name'],
            icon=folium.Icon()
            ).add_to(mapa2)
    st.title("Mapas")
    st.header("¿Qué tipo de aeropuerto tiene uno mal geolocalizado?")
    mapa2 = generar_mapa()
    ac1,ac2 = st.columns([0.3, 0.7])
    r_areas = ac1.checkbox("Grandes")
    if r_areas:
        a_larg = aerp[aerp.type=='large_airport']
        a_larg.apply(agregar_marca_aerop2, axis=1)
    r_parques = ac1.checkbox("Medianos")
    if r_parques:
        a_med = aerp[aerp.type=='medium_airport']
        a_med.apply(agregar_marca_aerop2, axis=1)
    r_monu = ac1.checkbox("Pequeños")
    if r_monu:
        a_small = aerp[aerp.type=='small_airport']
        a_small.apply(agregar_marca_aerop2, axis=1)
    with ac2:
        st_folium(mapa2, key='aeropuertos2')
@st.cache_data
def data():
    todas = ['BUENOS AIRES', 'CABA', 'CORDOBA', 'JUJUY', 'SAN LUIS', 'FORMOSA',
       'CATAMARCA', 'LA PAMPA', 'MENDOZA', 'NEUQUEN', 'RIO NEGRO',
       'SAN JUAN', 'SANTA CRUZ', 'SANTA FE', 'SANTIAGO DEL ESTERO',
       'TUCUMAN', 'MISIONES', 'LA RIOJA', 'SALTA', 'CHACO',
       'TIERRA DEL FUEGO', 'CHUBUT', 'CORRIENTES', 'ENTRE RIOS']
    aeropuertos_prov = ['JUJUY', 'SAN LUIS', 'MENDOZA', 'NEUQUEN', 'RIO NEGRO',
        'SALTA',  'CHUBUT']
    aerp_sin = [elemento for elemento in todas if elemento not in aeropuertos_prov]
    seleccion_random = random.sample(aeropuertos_prov, 7)
    elijo_uno_falso = random.choice(aerp_sin)
    seleccion_random.append(elijo_uno_falso)
    
    shuffle(seleccion_random)
    return (seleccion_random, elijo_uno_falso)
    
with tab5:
    st.subheader(" 🚨 Mostrar en el mapa los aeropuertos medianos y de elevación mayor a 2000")
    with st.expander("¿En qué zona de Argentina creen que van a estar?"):
        mostrar,resp_incorrecta= data()
        #######################################['JUJUY', 'SAN LUIS', 'MENDOZA', 'NEUQUEN', 'RIO NEGRO',
        resp_ubi = st.form(key='ubi')
        verdaderas = mostrar.copy()
        verdaderas.remove(resp_incorrecta)
        with resp_ubi:#######################################'SALTA',  'CHUBUT']       
            #re = st.checkbox(":rainbow[Elige una de las que aparecen]",options=mostrar, index=None,)
            selected_options = []
            for option in mostrar:
                if st.checkbox(option):
                    selected_options.append(option)
            
            submitted = st.form_submit_button("Responder")
            if submitted:
                if resp_incorrecta in selected_options:
                    st.error("Umm una opción no es correcta.")
                elif all(option in verdaderas for option in selected_options) and len(selected_options) == len(verdaderas):
                    st.balloons()
                    st.success("¡Todas las respuestas son correctas!")
                else:
                    st.error("Faltan algunas respuestas correctas.")
    
