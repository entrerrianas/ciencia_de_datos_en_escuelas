import streamlit as st
from data.config import read_dataset
import random
import matplotlib.pyplot as plt




aerp = read_dataset('ar-airports.csv')
def datos_dataset(dat_df):
    data_error = list(dat_df.isnull().sum().sort_values(ascending=False)[1:5].values)
    num = dat_df.isnull().sum().max()
    data_error.append(num)

    random.shuffle(data_error)
    return (data_error,num)

tab0, tab1,tab2,tab3,tab4= st.tabs(['Inicio','Recordemos', '✅ Ejercicio 1', 'Analizamos',
                              '✅ Ejercicio 2'])
with tab0:
    st.image('images/banner.png')
    st.title("Parte 4")
    st.subheader("Bajate el archivo desde esta url")
    st.header("https://tinyurl.com/3ddm9eej")
with tab1:
    st.header("Recordemos")
    st.header("https://tinyurl.com/3ddm9eej")
    with st.expander("Importar la librería pandas"):
        code = '''import pandas as pd'''
        st.code(code, language='python')
    with st.expander("Abrir el dataset"):
        code = '''aeropuertos = pd.read_csv('ar-airports.csv')'''
        st.code(code, language='python')
        
    with st.expander("Conocer los nombres de las columnas"):
        code = '''aeropuertos.columns'''
        st.code(code, language='python')
    with st.expander("Queremos saber los aeropuertos que son pequeños"):
        code = '''aeropuertos[(aeropuertos.type=='small_airport')]'''
        st.code(code, language='python')  
    with st.expander("Queremos saber los aeropuertos pequeños que están a más de 400 ft"):
        code = '''aeropuertos[(aeropuertos.type=='small_airport') & (aeropuertos.elevation_ft<400)]'''
        st.code(code, language='python')
    
with tab2:     
    c1, c2 = st.columns(2)
    with c1.expander("¿Es cierto?"):
        st.write("De todos los aeropuertos de tipo **small_airport**, solamente hay **3** que tienen elevación mayor a **10000**")
    with c2.expander("Respuesta"):
        st.write("Respuesta correcta: **NO, ¿por qué?**")

with tab3:
    st.header("Valores nulos")
    with st.expander("Obtener la suma la cantidad de valores nulos de cada columna del dataframe"):
        code = '''aeropuertos.isnull().sum()'''
        st.code(code, language='python')
          
    with st.expander("¿Cuál es la columna con mayor cantidad de valores nulos?"):
        st.write("Respuesta correcta: **home_link       929**")
    with st.expander("¿Cuál es la columna con 35 valores nulos?"):
        st.write("Respuesta correcta: **elevation_ft**")     
    with st.expander("¿Cuántas filas tienen valores nulos del tipo de aeropuerto **small_airport**?"):
        st.write("Respuesta correcta: **32 rows**")      
with tab4:
    st.header("¿Es verdad?")
    c1, c2 , c3 = st.columns(3)
    with c1.expander("Pregunta 1"):
        st.write("¿Hay 223 aeropuertos que tienen elevación **menor o igual a 131**?")
    with c2.expander("Respuesta"):
        st.write("Respuesta correcta: **No, hay 227 rows**")
    with c3.expander("Tip"):
        code = '''aeropuertos[aeropuertos.elevation_ft<=131]]'''
        st.code(code, language='python')
    with c1.expander("Pregunta 2"):
        st.write("Todos los aeropuertos de tipo **large_airport** tienen elevación menor a **68**")
        
    with c2.expander("Respuesta"):
        st.write("Respuesta correcta: **si** 67 y 18")
    with c3.expander("Tip"):
        code = '''aeropuertos[(aeropuertos.type=='large_airport')]'''
        st.code(code, language='python')
