import pandas as pd
from data.config import read_dataset
import streamlit as st
import matplotlib.pyplot as plt
file_data = read_dataset('lagos_arg.csv')
st.title('Lagos de Chubut')
with st.expander("¿Cuáles son los lagos de Chubut?"):
    
    data = file_data[file_data.Ubicación=='Chubut']
    st.dataframe(data)