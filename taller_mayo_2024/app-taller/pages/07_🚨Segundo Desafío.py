import streamlit as st
import pandas as pd
from data.config import read_dataset
import streamlit as st
from random import shuffle
@st.cache_data
def data(prov, sup):
       file_data = read_dataset('lagos_arg.csv')

       datos_bien = file_data[(file_data.Ubicación==prov) & (file_data["Superficie (km²)"]>sup)].Nombre.values[0]

       datos_no =   list(file_data[(file_data.Ubicación=='Santa Cruz') & (file_data["Superficie (km²)"]<100)].Nombre.values)
       shuffle(datos_no)
       lista_respuestas = datos_no[:5]
       
       lista_respuestas.append(datos_bien)
       shuffle(lista_respuestas)
       return (lista_respuestas, datos_bien)
tab1, tab2 =st.tabs(['Desafío 1', 'Desafío 2'])
with tab1:
       st.subheader(" 🚨 ¿Cuál es el nombre del lago de Chubut mayor a 100 km*2")
       

       mostrar,resp_correcta = data('Chubut', 100)
       #######################################3#resp correcta: 'Lago Musters']              
       re = st.radio(":rainbow[Elige uno]",options=mostrar, index=None,)
      
       if not re==None and re==resp_correcta:
              st.balloons()
              mensaje = 'Muy bueno!!'     
              
       else:
              mensaje = 'Umm hay que pensarlo un poco  más'

       if not re==None:
              st.header(mensaje)
with tab2:
       st.subheader(" 🚨 ¿Cuál es el nombre del lago de Tierra del Fuego, Antártida e Islas del Atlántico Sur mayor a 40 km*2")
       mostrar,resp_correcta = data('Tierra del Fuego, Antártida e Islas del Atlántico Sur', 40)
      
       #######################################3#resp correcta: ''Lago Yehuin']              
       re = st.radio(":rainbow[Elige uno]",options=mostrar, index=None,)

       if not re==None and re==resp_correcta:
              st.balloons()
              mensaje = 'Muy bueno!!'     
              
       else:
              mensaje = 'Umm hay que pensarlo un poco  más'

       if not re==None:
              st.header(mensaje)