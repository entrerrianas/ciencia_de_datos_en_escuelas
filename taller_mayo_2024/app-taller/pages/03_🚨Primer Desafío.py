import streamlit as st
import pandas as pd
from data.config import read_dataset
import streamlit as st

lagos = read_dataset('lagos_arg.csv')
st.title("Parte 1")
st.header("Datos que encontraron")
filas, columnas = lagos.shape
#st.write(f' filas: 52{filas} y columnas: {columnas}')
# 52 y 6
with st.expander("¿Cuántas filas y columnas encontraron?"):
    respuesta = st.form(key='filas_col')
    with respuesta:
        filas_resp = st.number_input(f'Filas', key='filas_k',
                                      min_value=1, max_value=5000
                                        )
        
        col_resp = st.number_input(f'Columnas', key='col_k',
                                   min_value=1, max_value=5000)
        
        submitted = st.form_submit_button("Responder")
    
    if submitted:
        mensaje = ''
        if filas_resp==filas and col_resp==columnas:
            st.balloons()
            mensaje = 'Muy bueno!!'
        elif filas_resp==filas:
            mensaje += 'Correcto cant filas'
            if not col_resp==columnas:
                mensaje += f' e incorrecto cant columnas'
            
        else:
            mensaje +='Incorrecto cant filas'
            if not col_resp==columnas:
                mensaje += f' e incorrecto cant columnas'
            else: 
                mensaje += f' y correcto cant columnas'
            
        
            
        st.subheader(mensaje)
nombres_ubic = lagos.Ubicación.unique()
cant_ubic = len(lagos.Ubicación.unique())
#st.write(f'{cant_ubic=}')
#9
with st.expander("¿Cuántos son los valores únicos de la columna **Ubicación**?"):
    resp_ubi = st.form(key='ubi')
    with resp_ubi:
        num_ubi = st.number_input(f'Cantidad', key='cant_k',
                                      min_value=1, max_value=5000
                                        )
        
        
        submitted = st.form_submit_button("Responder")
    
    if submitted:
        mensaje = ''
        if num_ubi==cant_ubic :
            st.balloons()
            mensaje = 'Muy bueno!!'
        else:
            mensaje = 'Ummmm....nop'
            
        st.subheader(mensaje)
with st.expander("¿Cuáles son los valores únicos de la columna **Ubicación**?"):
    c1,c2 =st.columns(2)
    
    #nombres_ubic
    otras = ['Entre Ríos', 'Formosa', 'La Pampa']
    resp_ubi = st.form(key='names_ubi')
    with resp_ubi:
        resp_list = {}
        for ubiq in nombres_ubic:
            r = c1.checkbox(ubiq)
            resp_list[ubiq]=r
        for ubiq in otras:
            
            r = (c1.checkbox(ubiq))    
            resp_list[ubiq]=r
        submitted_ubi = st.form_submit_button("Responder")
    
    if submitted_ubi:
        #st.write(nombres_ubic)
        #st.write(resp_list)
        diccionarios_filtrados = [d for d in resp_list if resp_list[d]]
        #st.write(diccionarios_filtrados)
        
        if all(i in nombres_ubic for i in diccionarios_filtrados) and \
                any(i not in otras for i in diccionarios_filtrados):
            st.balloons()
        else:
            st.subheader('Respondiste....alguno está mal ')
            st.write(diccionarios_filtrados)
        