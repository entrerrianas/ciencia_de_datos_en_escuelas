import streamlit as st
import pandas as pd
from data.config import read_dataset
import streamlit as st
import matplotlib.pyplot as plt
import folium
from streamlit_folium import folium_static, st_folium

tab1,tab2,tab3,tab4 = st.tabs(['Aeropuertos Argentina', 'Áreas protegidas', ' Conectividad Argentina',
                              'Avistajes de felinos'])
def generar_mapa():
    attr = (
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> '
        'contributors, &copy; <a href="https://cartodb.com/attributions">CartoDB</a>'
    )
    
    tiles = 'https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/capabaseargenmap@EPSG%3A3857@png/{z}/{x}/{-y}.png'
    m = folium.Map(
        location=(-33.457606, -65.346857),
        control_scale=True,
        zoom_start=5,
        name='es',
        tiles=tiles,
        attr=attr
    )
    return m
with tab1:
    aeropuertos = read_dataset('ar-airports.csv')
    st.title("Mapa")
    mapa1 = generar_mapa()
    def agregar_marca_aerop(row):
        folium.Marker(
            [row['lat'], row['lng']],
            popup=row['name'],
            icon=folium.Icon()
            ).add_to(mapa1)
    
    ac1,ac2 = st.columns([0.3, 0.7])
    r_areas = ac1.checkbox("Grandes")
    if r_areas:
        a_larg = aeropuertos[aeropuertos.type=='large_airport']
        a_larg.apply(agregar_marca_aerop, axis=1)
    r_parques = ac1.checkbox("Medianos")
    if r_parques:
        a_med = aeropuertos[aeropuertos.type=='medium_airport']
        a_med.apply(agregar_marca_aerop, axis=1)
    r_monu = ac1.checkbox("Pequeños")
    if r_monu:
        a_small = aeropuertos[aeropuertos.type=='small_airport']
        a_small.apply(agregar_marca_aerop, axis=1)
    with ac2:
        st_folium(mapa1, key='aeropuertos')
    
    
with tab2:
    
    def read_reser():
        areas_df = read_dataset('area_protegida.csv')
        return areas_df
    st.title("Mapa")
    areas = read_reser()
    mapa2 = generar_mapa()
    
    @st.cache_data
    def agregar_marca_areas(row):
        folium.Marker(
        [row['lat'], row['lng']],
        popup=row['nam'],
        icon=folium.Icon()
        ).add_to(mapa2)
    c1,c2 = st.columns([0.3, 0.7])
    r_areas = c1.checkbox("Reservas protegidas")
    if r_areas:
        areas_r = areas[areas.tap==2]
        areas_r.apply(agregar_marca_areas, axis=1)
    r_parques = c1.checkbox("Parques")
    if r_parques:
        areas_p = areas[areas.tap==1]
        areas_p.apply(agregar_marca_areas, axis=1)
    r_monu = c1.checkbox("Monumentos")
    if r_monu:
        areas_m = areas[areas.tap==3]
        areas_m.apply(agregar_marca_areas, axis=1)
    with c2:
        st_folium(mapa2, key='areas')
     

   
with tab3:
    conectividad = read_dataset('Conectividad_Internet.csv')
    st.title("Mapa  de Tecnología")
    mapa3 = generar_mapa()
    def agregar_marca_conect(row):
        folium.Marker(
        [row['lat'], row['lng']],
        popup=row['Localidad'],
        icon=folium.Icon()
        ).add_to(mapa3)
    tecnologias = ['ADSL','CABLEMODEM','DIALUP','FIBRAOPTICA','SATELITAL',
                   'WIRELESS','TELEFONIAFIJA']
    
   

    form_conect = st.form('Elegir los parametros para mostrar el mapa')
    with form_conect:
        c1, c2 = st.columns(2)
        tec_op = c1.selectbox("Elige la tecnologia a mostrar", key='ta',options=tecnologias)
        
        if 'ta' in st.session_state:            
            conectividad_map  = conectividad[conectividad[st.session_state['ta']]=='SI']
        provinces = conectividad_map.Provincia.unique()
        prov_filtradas = c2.multiselect("elija las provincias", provinces, key='prov_filtro')
        submit_airport = st.form_submit_button('Mostrar')
    
        if submit_airport:
            st.write(st.session_state['prov_filtro'])
            conectividad_map = conectividad_map[conectividad_map.Provincia.isin(prov_filtradas)]
            conectividad_map.apply(agregar_marca_conect, axis=1)
            st_folium(mapa3)
with tab4:
    felinos = read_dataset('felinos_filtrado.csv')
    fig4, ax4 = plt.subplots()
    conteo_ubicaciones = felinos['species'].value_counts()
    labels = list(conteo_ubicaciones.index)
    fruits = ['apple', 'blueberry', 'cherry', 'orange']
    counts = [40, 100, 30, 55]
    bar_labels = ['red', 'blue', '_red', 'orange']
    bar_colors = ['tab:red', 'tab:blue', 'tab:red', 'tab:orange']

    ax4.bar(conteo_ubicaciones.index, conteo_ubicaciones.values)

    ax4.set_ylabel('Cantidad')
    ax4.set_title('Cantidad vistos por especie')
    
    ax4.set_xticklabels(labels, rotation=45)

    st.pyplot(fig4)