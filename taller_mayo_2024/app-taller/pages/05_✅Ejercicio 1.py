import streamlit as st
import pandas as pd
from data.config import read_dataset
import streamlit as st
import matplotlib.pyplot as plt

tab1,tab2,tab3,tab4 = st.tabs(['Aeropuertos Argentina', 'Áreas protegidas', ' Conectividad Argentina',
                              'Avistajes de felinos'])
with tab1:
    aeropuertos = read_dataset('ar-airports.csv')
    st.title("Gráfico")
    fig,ax = plt.subplots()
    # Crear el gráfico de barras
    conteo_tipos = aeropuertos['type'].value_counts()
    conteo_tipos.plot(kind='bar', ax=ax, color='skyblue')
    # Agregar etiquetas y título
    ax.set_xlabel('Tipos de aeropuertos')
    ax.set_ylabel('Cantidad')
    ax.set_title('Cantidad de Aeropuertos por Tipo')

    # Mostrar el gráfico
    st.pyplot(fig)
with tab2:
    areas = read_dataset('area_protegida.csv')
    st.title("Gráfico")
    fig,ax = plt.subplots()
    labels = ['reservas', 'parques', 'monumentos', 'no disponible']
    # Crear el gráfico de barras
    conteo_ubicaciones = areas['tap'].value_counts()
    #conteo_ubicaciones.plot(kind='bar', ax=ax, color='skyblue', label=labels)
    ax.bar(conteo_ubicaciones.index, conteo_ubicaciones.values, label=labels, color='skyblue')

    # Agregar etiquetas y título
    ax.set_xlabel('Tipo')
    ax.set_ylabel('Cantidad de Areas protegidas')
    ax.set_title('Cantidad de Áreas por tipo de: parques, reservas, monumentos')

    # Mostrar el gráfico
    st.pyplot(fig)
with tab3:
    conectividad = read_dataset('Conectividad_Internet.csv')
    st.title("Gráfico")
    fig3,ax3 = plt.subplots()
    conteo_tipos = conectividad['FIBRAOPTICA'].value_counts()
    conteo_tipos.plot(kind='pie', ax=ax3, color='skyblue')

    # Agregar etiquetas y título

    ax3.set_ylabel('Porcentajes')
    ax3.set_title('Cantidad con Fibra óptica')
    st.pyplot(fig3)
with tab4:
    felinos = read_dataset('felinos_filtrado.csv')
    fig4, ax4 = plt.subplots()
    conteo_ubicaciones = felinos['species'].value_counts()
    labels = list(conteo_ubicaciones.index)
    fruits = ['apple', 'blueberry', 'cherry', 'orange']
    counts = [40, 100, 30, 55]
    bar_labels = ['red', 'blue', '_red', 'orange']
    bar_colors = ['tab:red', 'tab:blue', 'tab:red', 'tab:orange']

    ax4.bar(conteo_ubicaciones.index, conteo_ubicaciones.values)

    ax4.set_ylabel('Cantidad')
    ax4.set_title('Cantidad vistos por especie')
    
    ax4.set_xticklabels(labels, rotation=45)

    st.pyplot(fig4)