import streamlit as st
from data.config import load_config, save_config
from data.grupos import validate_group
st.title("Parte 1")
st.header("📂Descarga este archivo y contanos")
st.write("¿Qué sabemos de los datos")

tab1, tab2 = st.tabs(['Datasets', 'Preguntas'])

with tab1:
    inputs = ['Grupo1', 'Grupo2', 'Grupo3', 'Grupo4', 'Grupo5', 'Grupo6', 'Grupo7',
                'Grupo8', 'Grupo9', 'Grupo10', 'Grupo11']
    escuelas = ['Técnica1','Técnica 9' ]
    input_value = st.selectbox(f"Selecciones el grupo ", inputs, index=None)
    opciones_select = ['Aeropuertos Argentina', 'Áreas protegidas', 'Conectivdad Argentina',
                        'Lagos Argentinos','Datos sobre consumo cultural','Avistajes de felinos en Argentina',
                        ]        
    escuela_seleccionada = st.selectbox(f"Seleccione la escuela de {input_value}", escuelas, index=None)
    descripcion = st.text_input(f'Identificacion del grupo {input_value}')    
    select_values = st.selectbox(f"Seleccione una opción para {input_value}", opciones_select, index=None)
    agregar = st.button("Agregar")
    if agregar:
        st.write("Valores asociados:")
        st.write(f"{input_value}:  {select_values}")
        if input_value not in st.session_state:
            st.session_state[input_value] = {'dataset':select_values, 
                                             'descripcion': descripcion,
                                             'escuela': escuela_seleccionada}
        else:
            st.session_state[input_value] = {'dataset':select_values, 'descripcion': descripcion}
        st.write(st.session_state)
        
        config = load_config()
        valid, error = validate_group(input_value)
        if valid:
            config['groups'].append(
                { 'group': input_value,
                'dataset':select_values, 
                'descripcion': descripcion,
                'escuela': escuela_seleccionada
            })
        else:
            for group in config['groups']:
                if group['group'] == input_value:
                    group['dataset'] = select_values
                    group['descripcion'] = descripcion
                    group['escuela'] = escuela_seleccionada
                    break
        save_config(config)
        st.write(config)

        

with tab2:
    st.write('seguir')