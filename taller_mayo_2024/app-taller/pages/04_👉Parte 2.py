import streamlit as st
import pandas as pd
from data.config import read_dataset
import streamlit as st
st.title('Parte 2 del Taller')
st.header('¿Qué podemos saber de nuestro dataset?')
dataset = [
    {'Nombre':'Aeropuertos Argentina', 'columna': 'type','cant_col':0, 'cant_filas':0, 'cant_nulos':0, 'unicos':0},
    {'Nombre': 'Áreas protegidas','columna': 'tap','cant_col':0, 'cant_filas':0, 'cant_nulos':0, 'unicos':0},
    {'Nombre': 'Conectivdad Argentina', 'columna': 'FIBRAOPTICA','cant_col':0, 'cant_filas':0, 'cant_nulos':0, 'unicos':0},
    {'Nombre': 'Datos sobre consumo cultural', 'columna': 'p4','cant_col':0, 'cant_filas':0, 'cant_nulos':0, 'unicos':0},
    {'Nombre': 'Avistajes de felinos en Argentina','columna':'species','cant_col':0, 'cant_filas':0, 'cant_nulos':0, 'unicos':0}
]
dic_name_arch = {'Aeropuertos Argentina': 'ar-airports.csv',
                 'Áreas protegidas':'area_protegida.csv',
                 'Conectivdad Argentina': 'Conectividad_Internet.csv',
                 'Datos sobre consumo cultural': 'encc_2017.csv',
                 'Avistajes de felinos en Argentina':'felinos_filtrado.csv'}
dataset_new = []
df = pd.DataFrame(dataset)
edited_df = st.data_editor(df, num_rows="dynamic")

#favorite_command = edited_df.loc[edited_df["cant_col"].idxmax()]["Nombre"]
#st.markdown(f"Your favorite command is **{favorite_command}** 🎈")

#aerp = read_dataset('ar-airports.csv')
def datos_dataset(dat_df,col_ind):
    d_f =  read_dataset(dic_name_arch[dat_df])
    f,c = d_f.shape
    cant_null = max(d_f.isnull().sum())
    val_uniq = len(d_f[col_ind].unique())
    return (f,c,cant_null,val_uniq)
for data_sin_datos in dataset:
    nombre_dataset = data_sin_datos['Nombre']
    columna_indicada = data_sin_datos['columna']
    f,c, cant_null, cant_unicos = datos_dataset(nombre_dataset, columna_indicada)
    dataset_new.append({'Nombre': nombre_dataset ,'cant_col':c, 
                        'cant_filas':f, 'cant_nulos':cant_null, 
                        'columna':columna_indicada, 'unicos': cant_unicos}  )
    
    

df_new = pd.DataFrame(dataset_new)
with st.expander("¿Cuántas filas y columnas encontraron?"):
   
    st.write(df_new)
    