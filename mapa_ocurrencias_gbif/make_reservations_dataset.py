from shapely.geometry import Point
import geopandas as gpd
import pandas as pd
from pathlib import Path

BASE_PATH = Path(__file__).parent
DATA_PATH = BASE_PATH / "datasets"

AREAS_PATH = DATA_PATH / "area_protegida.json"
ANIMALS_PATH = DATA_PATH / "animals_multimedia.csv"

COLS = [
    "decimalLatitude",
    "decimalLongitude",
    "vernacularName",
    "verbatimScientificName",
    "iucnRedListCategory",
    "image",
]
TYPES = {
    "decimalLatitude": float,
    "decimalLongitude": float,
    "vernacularName": str,
    "verbatimScientificName": str,
    "iucnRedListCategory": str,
    "image": str,
}


def divide_dataset():

    animals_data = pd.read_csv(
        ANIMALS_PATH,
        encoding="utf-8",
        engine="c",
        usecols=COLS,
        dtype=TYPES,

    )

    geojson_file = AREAS_PATH
    gdf = gpd.read_file(geojson_file)
    gdf["id"] = gdf.index.astype(str)

    animals_data["location"] = animals_data.apply(
        lambda row: Point(row["decimalLongitude"], row["decimalLatitude"]), axis=1
    )

    #ocurrences_inside_reservations = animals_data[animals_data["location"].apply(lambda loc: any(gdf.geometry.contains(loc)))]
    #ocurrences_inside_reservations.to_csv(DATA_PATH / "ocurrences_inside_reservations.csv",mode="w", index=False, encoding="utf-8")
    ocurrences_outside_reservations = animals_data[animals_data["location"].apply(lambda loc: not any(gdf.geometry.contains(loc)))]
    ocurrences_outside_reservations.to_csv(DATA_PATH / "ocurrences_outside_reservations.csv",mode="w", index=False, encoding="utf-8")


divide_dataset()
