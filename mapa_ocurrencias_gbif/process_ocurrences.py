import pandas as pd
import csv
from pathlib import Path
from shapely.geometry import Point

BASE_PATH = Path(__file__).parent
DATA_PATH = BASE_PATH / "datasets"
GBIF_PATH = DATA_PATH / "gbif"
CHUNK_SIZE = 100000

COLUMNS = ["gbifID","catalogNumber","year","month","day","eventTime","decimalLongitude","decimalLatitude","class","order","family","verbatimScientificName","vernacularName","taxonKey","level1Name","level2Name","iucnRedListCategory"]

NO_NA_COLUMNS = ["gbifID","catalogNumber","year","month","day","decimalLongitude","decimalLatitude","class","order","family","verbatimScientificName","taxonKey","level1Name","level2Name","iucnRedListCategory"]

def process_csv_chunked(path,func,chunk_size=CHUNK_SIZE, delimiter="," , columns=""):
    i = 0
    header_written = False
    for chunk in pd.read_csv(path, chunksize=chunk_size, usecols=COLUMNS, encoding="utf-8", engine='c',delimiter=delimiter):
        if header_written:
            func(chunk, write_header=False)
        else:
            func(chunk, write_header=True)
            header_written = True
        i += 1
        print(f"chunk {i} done")

def cut(chunk, write_header):
    chunk.dropna(subset=NO_NA_COLUMNS, inplace=True)
    if write_header:
        chunk.to_csv(DATA_PATH / "animals_filtered.csv",mode="w", index=False, encoding="utf-8")
    else:
        chunk.to_csv(DATA_PATH / "animals_filtered.csv",mode="a", index=False, encoding="utf-8", header=False)


media_df = pd.read_csv(DATA_PATH / "multimedia.csv", encoding="utf-8", engine='c')
media_df.rename(columns={"identifier":"image"}, inplace=True)

def merge(chunk, write_header):
    merged_df = pd.merge(chunk, media_df, on="gbifID", how="left")
    if write_header:
        merged_df.to_csv(DATA_PATH / "animals_multimedia.csv",mode="w", index=False, encoding="utf-8")
    else:
        merged_df.to_csv(DATA_PATH / "animals_multimedia.csv",mode="a", index=False, encoding="utf-8", header=False)

def show(chunk):
    print(chunk.head())

process_csv_chunked(GBIF_PATH / "occurrence.txt", cut,columns=COLUMNS, delimiter="\t")
process_csv_chunked(DATA_PATH / "animals_filtered.csv", merge,columns=COLUMNS)

