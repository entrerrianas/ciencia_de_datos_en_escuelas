import pandas as pd
import plotly.graph_objects as go
from pathlib import Path
import requests
import json

BASE_PATH = Path.cwd()
DATA_PATH = BASE_PATH / "datasets"

AREAS_PATH = DATA_PATH / "area_protegida.json"
ANIMALS_PATH = DATA_PATH / "ocurrences_inside_reservations.csv"

COLS = [
    "decimalLatitude",
    "decimalLongitude",
    "vernacularName",
    "catalogNumber",
    "verbatimScientificName",
    "iucnRedListCategory",
    "image",
]
TYPES = {
    "decimalLatitude": float,
    "decimalLongitude": float,
    "catalogNumber": str,
    "vernacularName": str,
    "verbatimScientificName": str,
    "iucnRedListCategory": str,
    "image": str,
}

ARGENMAP_STYLE = {
    "version": 8,
    "sources": {
        "argenmap": {
            "type": "raster",
            "scheme": "tms",
            "tiles": [
                "https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/capabaseargenmap@EPSG%3A3857@png/{z}/{x}/{y}.png"
            ],
            "tileSize": 256,
        }
    },
    "layers": [
        {
            "id": "imagery-tiles",
            "type": "raster",
            "source": "argenmap",
            "below": "waterway-label",
            "minzoom": 1,
            "maxzoom": 18,
        }
    ],
}

IUCN_COLORS = {
    "CR": "red",
    "EN": "orange",
    "VU": "yellow",
    "NT": "green",
    "LC": "blue",
    "DD": "gray",
    "NE": "black",
}
entered = 0
requests_amount = 0

def get_name(row, names_dict):
    global entered 
    entered += 1
    if str(row["vernacularName"]) != "nan":
        return row["vernacularName"]
    if row["catalogNumber"] in names_dict:
        return names_dict[row["catalogNumber"]]
    global requests_amount
    requests_amount += 1
    r = requests.get(f"https://api.inaturalist.org/v1/observations/{row["catalogNumber"]}")
    try:
        name = json.loads(r.content.decode("utf-8"))["results"][0]["taxon"]["preferred_common_name"]
        names_dict[row["catalogNumber"]] = name
    except:
        print(f"Error with {row['catalogNumber']}")
        name = "No disponible"
    row["vernacularName"] = name
    print(f"Name: {row['vernacularName']}")
    return name

def filter_data(animals_data):
    filtered_data = animals_data[animals_data.iucnRedListCategory.isin(["CR", "EN","VU","NT"])]
    return filtered_data

def clean_data(data):
    names_dict = {}
    data.apply(lambda row: get_name(row, names_dict), axis=1)
    print(f"entered: {entered}, requests: {requests_amount}")
    return data

def endangered_animals_map(filtered_data):
    fig = go.Figure()
    fig = fig.update_layout(
        mapbox_style=ARGENMAP_STYLE,
        mapbox_center={"lat": -38.4161, "lon": -63.6167},
        mapbox_zoom=4,
    )
    fig.add_scattermapbox(
        customdata=filtered_data[["catalogNumber","vernacularName", "verbatimScientificName", "iucnRedListCategory", "image"]],
        lat=filtered_data["decimalLatitude"],
        lon=filtered_data["decimalLongitude"],
        marker=go.scattermapbox.Marker(
                color = filtered_data["iucnRedListCategory"].map(IUCN_COLORS),
                size = 10, 
            ),
        hovertemplate="""
        <b>Common Name:</b> %{customdata[1]}<br>
        <b>Scientific Name:</b> %{customdata[2]}<br>
        <b>IUCN Red List Category:</b> %{customdata[3]}<br>
        <b>Latitude:</b> %{lat:.2f}<br>
        <b>Longitude:</b> %{lon:.2f}<br>
        <extra></extra>
        """,
    )

    return fig


animals_data = pd.read_csv(
    ANIMALS_PATH,
    encoding="utf-8",
    engine="c",
    usecols=COLS,
    dtype=TYPES,
)

#filtered_data = filter_data(animals_data)
#cleaned_data = clean_data(filtered_data)
#endangered_animals_map(filtered_data).write_html("inside_map.html")

print(pd.read_csv(DATA_PATH / "gbif" / "occurrence.txt", nrows=1, encoding="utf-8",sep="\t", usecols=["acceptedNameUsageID"], engine='c').head())