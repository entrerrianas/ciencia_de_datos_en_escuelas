import pandas as pd
from pathlib import Path
from process_dataset import process_csv_chunked
from multiprocessing import freeze_support

BASE_PATH = Path.cwd().parent
DATA_PATH = BASE_PATH / "datasets"
GBIF_PATH = DATA_PATH / "gbif"

COLUMNS = ["gbifID","catalogNumber","year","month","day","eventTime","decimalLongitude","decimalLatitude","class","order","family","verbatimScientificName","vernacularName","taxonKey","level1Name","level2Name","iucnRedListCategory"]

NO_NA_COLUMNS = ["gbifID","catalogNumber","year","month","day","decimalLongitude","decimalLatitude","class","order","family","verbatimScientificName","taxonKey","level1Name","level2Name","iucnRedListCategory"]


def cut(chunk):
    return chunk.dropna(subset=NO_NA_COLUMNS, inplace=False)

media_df = pd.read_csv(GBIF_PATH / "multimedia.txt", encoding="utf-8", engine='c', delimiter="\t", usecols=["gbifID","identifier"], low_memory=False)
media_df.rename(columns={"identifier":"image"}, inplace=True)
def merge(chunk):
    return pd.merge(chunk, media_df, on="gbifID", how="left")


if __name__ == '__main__':
    freeze_support()
    process_csv_chunked(GBIF_PATH / "occurrence.txt",DATA_PATH / "animals_filtered2.csv", cut,read_kwargs={"usecols":COLUMNS,"delimiter":"\t"} )
    process_csv_chunked(DATA_PATH / "animals_filtered.csv",DATA_PATH / "animals_multimedia2.csv", merge, read_kwargs={"usecols":COLUMNS},num_processes=8)


