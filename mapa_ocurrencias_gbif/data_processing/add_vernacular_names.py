from process_dataset import process_csv_chunked
from pathlib import Path
import requests
import json
import time

BASE_PATH = Path.cwd().parent
DATA_PATH = BASE_PATH / "datasets"
GBIF_PATH = DATA_PATH / "gbif"
#curl -X 'GET' 'https://api.gbif.org/v1/species/{taxonKey}/vernacularNames' -H 'accept: application/json'

def get_spanish_name(response):
    try:
        vernacular_names = response["results"]
        spanish_name = None
        for name in vernacular_names:
            if name["language"] == "spa":
                spanish_name = name["vernacularName"]
                break
        if spanish_name is not None:
            return spanish_name
        else:
            return "nan"
    except:
        return "nan"

def get_name(row, names_dict):
    if row["taxonKey"] in names_dict:
        return names_dict[row["taxonKey"]]
    try:
        r = requests.get(f"https://api.gbif.org/v1/species/{row['taxonKey']}/vernacularNames", headers={"accept": "application/json"})
        time.sleep(12)  # Wait for 12 seconds between requests
    except:
        print(f"Connection error, try later")
    try:
        name = get_spanish_name(json.loads(r.content.decode("utf-8")))
        if name != "nan":
            names_dict[row["catalogNumber"]] = name
    except:
        print(f"Error with {row['catalogNumber']}")
        name = "No disponible"
    row["vernacularName"] = name
    print(f"Name: {row['vernacularName']}")
    return name

def add_vernacular_names(chunk):
    names_dict = {}
    chunk["vernacularName"] = chunk.apply(lambda row: get_name(row, names_dict) if str(row["vernacularName"]) != "nan" else row["vernacularName"], axis=1)
    return chunk

print("Adding vernacular names to animals_multimedia")
#process_csv_chunked(DATA_PATH / "animals_multimedia.csv", DATA_PATH / "animals_multimedia_with_names.csv", add_vernacular_names,chunk_size=100)

r = requests.get(f"https://api.gbif.org/v1/species/{2485136}/vernacularNames", headers={"accept": "application/json"})


spanish_name = get_spanish_name(json.loads(r.content.decode("utf-8")))
print(f"Spanish Name: {spanish_name}")
