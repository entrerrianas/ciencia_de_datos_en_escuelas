import streamlit as st
import pandas as pd
from pathlib import Path

BASE_PATH = Path(__file__).parent
DATA_PATH = BASE_PATH / "datasets"


st.title("GBIF Animals")

ANIMALS_PATH = DATA_PATH / "animals_multimedia.csv"
dtypes_animals = {
    "gbifID": int,
    "decimalLatitude": object,
    "decimalLongitude": object,
    "taxonID": "Int64",
    "acceptedNameUsageID": "Int64",
    "phylum": object,
    "class": object,
    "order": object,
    "family": object,
    "genus": object,
    "taxonKey": "Int64",
    "phylumKey": "Int64",
    "classKey": "Int64",
    "orderKey": "Int64",
    "familyKey": "Int64",
    "genusKey": "Int64",
    "speciesKey": object,
    "species": object,
    "level1Name": object,
    "level2Name": object,
    "iucnRedListCategory": object,
    "eventDateTime": object,
    "image": object,
}
animals = pd.read_csv(ANIMALS_PATH, dtype=dtypes_animals, engine="c")
dtypes_areas = {
    "decimalLatitude": object,
    "decimalLongitude": object,
    "areaId": int,
    "fna": object,
    "gna": object,
    "nam": object,
    "tap": "Int64",
    "jap": "Int64",
    "caa": "Int64",
    "fdc": object,
    "sag": object,
    "area": "Int64",
}
AREAS_PATH = DATA_PATH / "protected_areas_filtered.csv"
areas = pd.read_csv(AREAS_PATH,dtype=dtypes_areas, engine="c")
st.write(areas.head())
