from shapely.geometry import Point
import geopandas as gpd
import pandas as pd
import plotly.express as px
from pathlib import Path
import json

BASE_PATH = Path(__file__).parent
DATA_PATH = BASE_PATH / "datasets"

AREAS_PATH = DATA_PATH / "area_protegida.json"
ANIMALS_PATH = DATA_PATH / "animals_multimedia.csv"

COLS = [
    "decimalLatitude",
    "decimalLongitude",
    "vernacularName",
    "verbatimScientificName",
    "iucnRedListCategory",
    "image",
]
TYPES = {
    "decimalLatitude": float,
    "decimalLongitude": float,
    "vernacularName": str,
    "verbatimScientificName": str,
    "iucnRedListCategory": str,
    "image": str,
}

ARGENMAP_STYLE = {
    "version": 8,
    "sources": {
        "argenmap": {
            "type": "raster",
            "scheme": "tms",
            "tiles": [
                "https://wms.ign.gob.ar/geoserver/gwc/service/tms/1.0.0/capabaseargenmap@EPSG%3A3857@png/{z}/{x}/{y}.png"
            ],
            "tileSize": 256,
        }
    },
    "layers": [
        {
            "id": "imagery-tiles",
            "type": "raster",
            "source": "argenmap",
            "below": "waterway-label",
            "minzoom": 1,
            "maxzoom": 18,
        }
    ],
}


def is_within_area(row, gdf):
    return any(
        gdf.geometry.contains(Point(row["decimalLongitude"], row["decimalLatitude"]))
    )


def filter_data(animals_data, gdf):
    filtered_data = animals_data[animals_data.iucnRedListCategory.isin(["CR", "EN"])]
    filtered_data = filtered_data[
        filtered_data.apply(lambda row: is_within_area(row, gdf), axis=1)
    ]
    return filtered_data


def endangered_animals_map_with_areas(filtered_data, gdf):
    gdf["id"] = gdf.index.astype(str)
    geojson_data = json.loads(gdf.to_json())

    fig = px.choropleth_mapbox(
        gdf,
        geojson=geojson_data,
        locations=gdf["id"],
        color=gdf["area"],
        color_continuous_scale="Viridis",
        range_color=(0, gdf["area"].max()),
        zoom=1,
        opacity=0.3,  # Add opacity to make the choropleth transparent
    )

    fig.add_scattermapbox(
        lat=filtered_data["decimalLatitude"],
        lon=filtered_data["decimalLongitude"],
        mode="markers",
        marker=dict(size=10, color="red", opacity=0.7),
        hovertemplate="<b>%{text}</b><br><br>"
        + "Latitud: %{lat:.2f}<br>"
        + "Longitud: %{lon:.2f}<br>"
        + "Red List Category: %{customdata[0]} <br>"
        + "Scientific Name: %{customdata[1]} <br>"
        + "Name: %{customdata[2]} <br>"
        + "<extra></extra>",
        text=filtered_data["verbatimScientificName"],
        customdata=filtered_data[
            ["iucnRedListCategory", "vernacularName", "verbatimScientificName"]
        ].values,
    )
    fig.update_layout(
        mapbox=dict(
            style=ARGENMAP_STYLE, center=dict(lon=-59.08574, lat=-46.83348), zoom=3
        )
    )
    fig.show()


def endangered_animals_map(filtered_data, gdf):
    gdf["id"] = gdf.index.astype(str)
    geojson_data = json.loads(gdf.to_json())
    fig = px.scatter_mapbox(
        lat=filtered_data["decimalLatitude"],
        lon=filtered_data["decimalLongitude"],
        hover_name=filtered_data["verbatimScientificName"],
        color_discrete_sequence=["red"],
    )
    fig.update_layout(
        mapbox=dict(
            style=ARGENMAP_STYLE, center=dict(lon=-59.08574, lat=-46.83348), zoom=5
        )
    )
    fig.show()


animals_data = pd.read_csv(
    ANIMALS_PATH,
    encoding="utf-8",
    engine="c",
    usecols=COLS,
    dtype=TYPES,
)
gdf = gpd.read_file(AREAS_PATH)
filtered_data = filter_data(animals_data, gdf)
endangered_animals_map(filtered_data, gdf)
endangered_animals_map_with_areas(filtered_data, gdf)
