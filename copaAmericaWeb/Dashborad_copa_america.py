import streamlit as st
import pandas as pd
import base64
import folium
import requests
from streamlit_folium import folium_static
import datetime
import time 
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import random
import asyncio
from graphviz import Digraph
     
st.set_page_config(layout="wide")
#URL="https://es.wikipedia.org/wiki/Anexo:Estad%C3%ADsticas_de_la_Copa_Am%C3%A9rica#Estad%C3%ADsticas_individuales"

datosJugadores=pd.read_csv('datosJugadores.csv')
copa_america_historico=pd.read_csv('copa_america_historico.csv')
campeones_invictos=pd.read_csv('campeones_invictos.csv')
equipo2024=pd.read_csv('Equipos2024.csv')
partidos = pd.read_csv('estadios_y_fechas.csv',dtype={"longitud": float,"latitud": float}, parse_dates=["fecha"],dayfirst=True)
goleadores_his=pd.read_csv('goleadores_historicos.csv') ##esto lo voy a usar para mostrarle a los pibes como sacar estadisticas
campeones_consecutivos=pd.read_csv('campeones_consecutivos.csv')
tabla_historica=pd.read_csv('tabla_historica.csv')
final=pd.read_csv('por_definir.csv')

# # Asegúrate de inicializar el estado
# if "stop_reloj" not in st.session_state:
#      st.session_state.stop_reloj = True


img_path = 'imagenes/logo-info.png'

with open(img_path, "rb") as imageFile:
    image_base64 = base64.b64encode(imageFile.read()).decode()
##imagen de la facultad
html = f"""
<button style='border: none; background: none;' onClick="location.href='/'">
            <img src='data:image/png;base64,{image_base64}' style='margin: auto; display: flex; height: 100px;'>
        </button>"""
st.sidebar.markdown(html, unsafe_allow_html=True)
equipos=[]
st.sidebar.title('Información Copa América')
for index,partido in tabla_historica.iterrows():
     equipos.append(partido['Equipo'])
rendimiento=st.sidebar.selectbox("Seleccione un equipo:", equipos)
tipo_seccion = st.sidebar.radio('Secciones:', ['Campeon 2024', 'Fase de grupo y sedes', 'Campeones consecutivos','Goleadores históricos',rendimiento],index=0)

# if "Campeon_2024" not in st.session_state:
#      st.session_state.Campeon_2024 = True
# if "gruposYFases" not in st.session_state:
#      st.session_state.gruposYFases = True
# if "consec" not in st.session_state:
#      st.session_state.consec = True
# if "rend" not in st.session_state:
#      st.session_state.rend = True
# if "golesH" not in st.session_state:
#      st.session_state.golesH = True     

argentina_posiciones=[]

def contador_regresivo():
    # Definimos la fecha objetivo
     fecha_objetivo = datetime.datetime(2026, 6, 20, 21, 0, 0)
     while True:
        # Obtenemos la fecha actual
         fecha_actual = datetime.datetime.now()

        # Calculamos la diferencia entre las fechas
         diferencia = fecha_objetivo - fecha_actual

        # Extraemos los días, horas, minutos y segundos de la diferencia
         dias = diferencia.days
         horas, segundos_restantes = divmod(diferencia.seconds, 3600)
         minutos, segundos = divmod(segundos_restantes, 60)

         yield dias, horas, minutos, segundos

         time.sleep(1)
def relojRegresivo():
     #Reloj regresivo para jugar la copa america 2024
     contador = contador_regresivo()
     display_container=st.empty()
     img_path = 'imagenes/logo2.png'
     with open(img_path, "rb") as imageFile:
         image_base64 = base64.b64encode(imageFile.read()).decode() 
     while not st.session_state.stop_reloj:
         dias, horas, minutos, segundos = next(contador)   
         ## html para el reloj regresivo del inicio junto con la imagen   
         
         html=f""" 
             
             <div style="background-color:#f0f0f0; padding: 10px; border-radius: 10px;">
              <img src='data:image/png;base64,{image_base64}' style='margin: auto; display: flex; height: 150px;'>
                 <div style="display:flex; justify-content:center; align-items:center">
                     <h3 style="color:blue; text-align:center; margin-right:30px">{dias}</h3>
                     <h3 style="color:blue; text-align:center; margin-right:30px">{horas}</h3>
                     <h3 style="color:blue; text-align:center; margin-right:30px">{minutos}</h3>
                     <h3 style="color:blue; text-align:center;margin-right:30px">{segundos}</h3>
                 </div>
                 <div style="display:flex; justify-content:center; align-items:center">
                     <h6 style="color:black; text-align:center; margin-right:10px";>días</h6>
                     <h6 style="color:black; text-align:center; margin-right:10px";>horas</h6>
                     <h6 style="color:black; text-align:center; margin-right:10px";>minutos</h6>
                     <h6 style="color:black; text-align:center;margin-right:10px";>segundos</h6>
                 </div>
             </div>
             """
         display_container.markdown(html, unsafe_allow_html=True)
         if st.session_state.get("stop_reloj"):
             break

def estadiosYFaseDeGrupos():
     st.header('Fase de grupos de la Copa América 2024')
     st.write('')
     st.write('')
     grupoA,grupoB,grupoC,grupoD=st.columns(4)
     
     lista_banderas=[]
     for index, row in equipo2024.iterrows():
         img_path = (f"imagenes/{row['Seleccion']}.png")
         with open(img_path, "rb") as imageFile:
             image_base64 = base64.b64encode(imageFile.read()).decode()
             lista_banderas.append(image_base64)
             print(lista_banderas)
     with grupoA:
         st.subheader('Grupo A')
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[0]}' style='width: 30px; height: 30px; border-radius: 50%;'> Argetina", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[11]}' style='width: 30px; height: 30px; border-radius: 50%;'> Peru", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[1]}' style='width: 30px; height: 30px; border-radius: 50%;'> Chile", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[14]}' style='width: 30px; height: 30px; border-radius: 50%;'> Canada", unsafe_allow_html=True)
     with grupoB:
         st.subheader('Grupo B')
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[8]}' style='width: 30px; height: 30px; border-radius: 50%;'> Mexico", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[9]}' style='width: 30px; height: 30px; border-radius: 50%;'> Jamaica", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[7]}' style='width: 30px; height: 30px; border-radius: 50%;'> Ecuador", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[13]}' style='width: 30px; height: 30px; border-radius: 50%;'> Venezuela", unsafe_allow_html=True)
     with grupoC:
         st.subheader('Grupo C')
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[2]}' style='width: 30px; height: 30px; border-radius: 50%;'> Estados Unidos", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[12]}' style='width: 30px; height: 30px; border-radius: 50%;'> Uruguay", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[3]}' style='width: 30px; height: 30px; border-radius: 50%;'> Panama", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[15]}' style='width: 30px; height: 30px; border-radius: 50%;'> Bolivia", unsafe_allow_html=True)
     with grupoD:
         st.subheader('Grupo D')
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[4]}' style='width: 30px; height: 30px; border-radius: 50%;'> Costa Rica", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[16]}' style='width: 30px; height: 30px; border-radius: 50%;'> Brasil", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[10]}' style='width: 30px; height: 30px; border-radius: 50%;'> Paraguay", unsafe_allow_html=True)
         st.markdown(f"<img src='data:image/png;base64,{lista_banderas[5]}' style='width: 30px; height: 30px; border-radius: 50%;'> Colombia", unsafe_allow_html=True)
     # Dividir la página en dos columnas
     st.write('')
     st.write('')
     st.header('En estos estadios se jugarán la final y la apertura de la Copa América 2024')
     col1, col2 = st.columns(2)
     
     #Columna con la imagen del estadio donde se hace la apertura a la copa america 2024
     with col1:
         st.markdown("<h1 style='color: blue'>Mercedes-Benz Stadium</h1>", unsafe_allow_html=True)
         st.image('imagenes/estadioMercedes.png')
         st.markdown('Atlanta,GA')
         st.markdown("<h5 style='color:red;'>⚽️ Apertura</h5>", unsafe_allow_html=True)
         st.markdown('')
         st.markdown("<h6 style='black: red'>Capacidad: 71.000</h6>", unsafe_allow_html=True)
     #Columna con la imagen del estadio donde se hace la apertura a la copa america 2024
     with col2:
         st.markdown("<h1 style='color: blue'>Hard Rock Stadium</h1>", unsafe_allow_html=True)
         st.image('imagenes/hardrock.png')
         st.markdown('Miami Gardens, FL')
         st.markdown("<h5 style='color:red;'>🏆 Final</h5>", unsafe_allow_html=True)
         st.markdown('')
         st.markdown("<h6 style='color:black;'>Capacidad: 65.300</h6>", unsafe_allow_html=True)


def crear_mapa():
     mapa = folium.Map(location=[37.0902, -95.7129], zoom_start=3)
     
     for index, partido in partidos.iterrows():
         marcador=folium.Marker([partido["latitud"], partido["longitud"]], popup=partido["estadios"]).add_to(mapa)
     return mapa
     
def fechas():
     partidos_hasta_26_junio =partidos[partidos['fecha'] <= '26-06-2024']
     partidos_hasta_2_julio=partidos[partidos["fecha"] > '26-06-2024']
     print(partidos_hasta_2_julio)
     col1,col2=st.columns(2)
     
     with col1:
         for index, partido in partidos_hasta_26_junio.iterrows():
             img_pathC = (f"""imagenes/{partido["paisC"]}.png""")
             img_pathS = (f"""imagenes/{partido["paisS"]}.png""")
             with open(img_pathC, "rb") as imageFile:
                 image_base64C = base64.b64encode(imageFile.read()).decode()
             with open(img_pathS, "rb") as imageFile:
                 image_base64S = base64.b64encode(imageFile.read()).decode()
             script = f"""
                 <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-bottom: 10px; width: 300px; text-align: center;">
                     <div style="font-size: 14px;">{partido["estadios"]}</div>
                     <div style="display: flex; align-items: center; justify-content: space-between; margin-top: 10px;">
                         <div style="flex: 1; display: flex; align-items: center;">
                             <img src='data:image/png;base64,{image_base64C}'  style="width: 20px; height: auto; margin-right: 5px;"> 
                             <span>{partido["paisC"]}</span>
                         </div>
                         <div style="flex: 0 1 auto;">{int(partido["golesC"])}</div>
                         <div style="flex: 0 1 auto;"> - </div>
                         <div style="flex: 0 1 auto;">{int(partido["golesS"])}</div>
                         <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                             <span>{partido["paisS"]}</span>
                             <img src='data:image/png;base64,{image_base64S}' style="width: 20px; height: auto; margin-left: 5px;">
                         </div>
                     </div>
                     <div style="margin-top: 10px;">{partido["fecha"].date()}</div>
                 </div>
                 """

             st.markdown(script,unsafe_allow_html=True)
     with col2:
         for index, partido in partidos_hasta_2_julio.iterrows():
             img_pathC = (f"""imagenes/{partido["paisC"]}.png""")
             img_pathS = (f"""imagenes/{partido["paisS"]}.png""")
             with open(img_pathC, "rb") as imageFile:
                 image_base64C = base64.b64encode(imageFile.read()).decode()
             with open(img_pathS, "rb") as imageFile:
                 image_base64S = base64.b64encode(imageFile.read()).decode()
             script = f"""
                 <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-bottom: 10px; width: 300px; text-align: center;">
                     <div style="font-size: 14px;">{partido["estadios"]}</div>
                     <div style="display: flex; align-items: center; justify-content: space-between; margin-top: 10px;">
                         <div style="flex: 1; display: flex; align-items: center;">
                             <img src='data:image/png;base64,{image_base64C}' style="width: 20px; height: auto; margin-right: 5px;">
                             <span>{partido["paisC"]}</span>
                         </div>
                         <div style="flex: 0 1 auto;">{int(partido["golesC"])}</div>
                         <div style="flex: 0 1 auto;"> - </div>
                         <div style="flex: 0 1 auto;">{int(partido["golesS"])}</div>
                         <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                             <span>{partido["paisS"]}</span>
                             <img src='data:image/png;base64,{image_base64S}' style="width: 20px; height: auto; margin-left: 5px;">
                         </div>
                     </div>
                     <div style="margin-top: 10px;">{partido["fecha"].date()}</div>
                 </div>
                 """

             st.markdown(script,unsafe_allow_html=True)

def rend(rendimiento):
     equipo=tabla_historica[tabla_historica["Equipo"]==rendimiento]
     fig1, ax1 = plt.subplots()
     ax1.pie([equipo["PE"].iloc[0],equipo["PG"].iloc[0],equipo["PP"].iloc[0]], labels=["Empatados", "Ganados", "Perdidos"], autopct='%1.1f%%', startangle=90)
     ax1.axis('equal')
     
     
     # Total de Copas América jugadas
     total_copas_americas = 47
    
     # Calcular el porcentaje de copas ganadas según las participaciones
     porcentajeTitls = (equipo['Titls'].iloc[0] / equipo['Part'].iloc[0])*100
     porcentajeNoTitls=100-porcentajeTitls
     
     # Calcular el porcentaje de participaciones de cada equipo en el total de copas jugadas
     porcentajePart = (equipo['Part'].iloc[0] / total_copas_americas)*100
     porcentajeNoPart =100 - porcentajePart
     
     # Plot de pie para porcentaje de copas ganadas
     fig2, ax1 = plt.subplots()
     ax1.pie([porcentajeTitls,porcentajeNoTitls], labels=["Titulos conseguidos","No conseguidos"], autopct='%1.1f%%', startangle=90)
     ax1.axis('equal')  # Para que el gráfico sea circular
     
     
     # Plot de pie para porcentaje de participaciones
     fig3, ax2 = plt.subplots()
     ax2.pie([porcentajePart,porcentajeNoPart], labels=["Participó","No participó"], autopct='%1.1f%%', startangle=90)
     ax2.axis('equal')  # Para que el gráfico sea circular
     
     Goles=equipo['GF'].iloc[0]
     GolesEnContra=equipo['GC'].iloc[0]


     fig4,ax4=plt.subplots()
     ax4.pie([Goles,GolesEnContra],labels=["Goles","Goles en contra"],autopct='%1.1f%%',startangle=90)
     ax4.axis('equal')
     
     st.title(f'Rendimiento de {equipo["Equipo"].iloc[0]}')
     col1, col2 = st.columns(2)
     # Mostrar los gráficos en Streamlit
     with col1:
         st.write(f'<br>Porcentaje de partidos ganados,empatados y perdidos por {equipo["Equipo"].iloc[0]}</br>',unsafe_allow_html=True)
         st.pyplot(fig1)
         st.write(f'<br>Goles a favor y en contra de {equipo["Equipo"].iloc[0]}</br>',unsafe_allow_html=True)
         st.pyplot(fig4)
     with col2:
         st.write(f'<br>Porcentaje de Titulos conseguidos en todas las Copas América por {equipo["Equipo"].iloc[0]}</br>',unsafe_allow_html=True)
         st.pyplot(fig2)
         st.write(f'<br>Porcentaje de participaciones en Copas América de {equipo["Equipo"].iloc[0]}</br>',unsafe_allow_html=True)
         st.pyplot(fig3)

import streamlit as st

def reset_session_state():
    # Reinicia el estado de session_state
    st.session_state.debut = ''
    st.session_state.posicion = ''
    st.session_state.pie_habil = ''
    st.session_state.apodo = ''
    st.session_state.jugador = ''
    st.session_state.pista_attempts = 0
    st.session_state.ganado = False
    st.session_state.initialized = False

def adivinador(red,jugadores):
     # Inicializar el estado solo si no está ya inicializado
     if 'initialized' not in st.session_state:
         reset_session_state()
         st.session_state.selected_country = red
 
     # Reiniciar estado si el país ha cambiado
     if st.session_state.selected_country != red or not st.session_state.initialized:      
         st.session_state.debut = jugadores['debut']
         st.session_state.posicion = jugadores['posicion']
         st.session_state.pie_habil = jugadores['pie_habil']
         st.session_state.apodo = jugadores['apodo']
         st.session_state.jugador = jugadores['nombre']
         st.session_state.pista_attempts = 0
         st.session_state.ganado = False
         st.session_state.initialized = True
         st.session_state.selected_country = red
         st.session_state.jugd = ''
 
     st.subheader(f'Adivinemos un jugador que jugó en la Selección de {red}')
     
     try:
         jugd = st.text_input('Ingresa el nombre completo del jugador', key='jugd')
         
         if jugd:
             if jugd == st.session_state.jugador:
                 st.balloons()
                 st.success('¡FELICITACIONES! Has adivinado el jugador.')
                 # Reiniciar el nombre ingresado
                 st.session_state.ganado = True
                 
             else:
                 st.warning('El nombre no es correcto. Sigue intentando.')
         if not st.session_state.ganado:
             # Mostrar información sobre el jugador
             st.info(f"Año de debut: {st.session_state.debut}")
             st.info(f"Posición en la cancha:  {st.session_state.posicion}")
             st.info(f"Pie habil: {st.session_state.pie_habil}")
             # Checkbox para la pista adicional
             pista = st.checkbox("¿Quieres una pista adicional?", key='pista')
             if pista:
                 st.info(f"El apodo del jugador es: {st.session_state.apodo}")
                 st.session_state.pista_attempts += 1
         else:
             jugd=st.empty()
 
     except TypeError as e:
         st.write(f'Error: {e}')
     except ValueError as v:
         st.write(f'Error: {v}')
     except Exception as ex:
         st.write(f'Error: {ex}')

def reset_session_state():
     # Reinicia el estado de session_state
     st.session_state.debut = ''
     st.session_state.posicion = ''
     st.session_state.pie_habil = ''
     st.session_state.apodo = ''
     st.session_state.jugador = ''
     st.session_state.pista_attempts = 0
     st.session_state.ganado = False
     st.session_state.initialized = False 



def estadisticaGoleadores():
    # Configuración inicial y extracción de datos
    st.title('Indaguemos sobre cómo obtener los datos')
    url = "https://es.wikipedia.org/wiki/Anexo:Estad%C3%ADsticas_de_la_Copa_Am%C3%A9rica#Estad%C3%ADsticas_individuales"
    goleadores = pd.read_html(url)
    goleadoresHist = goleadores[22]
    
    # Sección 1: Explicación y código
    col5, col6 = st.columns([2, 1])
    
    with col5:
        st.write('Los datos fueron extraídos de esta URL:')
        st.write(url)
        
        st.write('**¿Cómo hacemos para extraer los datos de una tabla de una página web?**')
        st.markdown("<small>Debemos seguir los siguientes pasos:</small>", unsafe_allow_html=True)
        
        st.markdown("<small>Primero, importamos la librería de Python <span style='color:green'><code>import pandas</code></span> para leer el HTML de la página:</small>", unsafe_allow_html=True)
        st.markdown("<span style='color:green; font-size: small;'>goleadores = pd.read_html(url)</span>", unsafe_allow_html=True)
        
        st.markdown("<small>La variable <span style='color:green'><code>url</code></span> es el enlace de la página anterior.</small>", unsafe_allow_html=True)
        st.markdown("<small>Luego, seleccionamos la tabla deseada (en este caso, la de goleadores):</small>", unsafe_allow_html=True)
        st.markdown("<span style='color:green; font-size: small;'>goleadoresHist = goleadores[22]</span>", unsafe_allow_html=True)
        
        st.markdown("<small>Podemos ver los datos obtenidos invocando la variable <span style='color:green'><code>goleadoresHist</code></span>.</small>", unsafe_allow_html=True)
        st.write('Esta tabla contiene los goleadores históricos de la Copa América.')
        
        st.markdown("<small>Con estos datos, podemos agrupar a los goleadores por país con la siguiente sentencia:</small>", unsafe_allow_html=True)
        st.markdown("<span style='color:green; font-size: small;'>goleadoresAgrupados = goleadoresHist.groupby('Selección').size()</span>", unsafe_allow_html=True)
        
        st.title('Podemos crear un gráfico de pastel con estos datos agrupados.')
    
    with col6:
        st.write(goleadoresHist)
        
    # Sección 2: Gráfico de pastel
    goleadoresAgrupados = goleadoresHist.groupby('Selección').size()
    col1, col2 = st.columns(2)
    
    with col1:
        st.markdown("<span style='color:green'>plt.figure(figsize=(6, 6))</span>", unsafe_allow_html=True)
        st.write('Crea una nueva figura para el gráfico, especificando el tamaño como 6x6 pulgadas.')
        
        st.markdown("<span style='color:green'>fig, ax = plt.subplots()</span>", unsafe_allow_html=True)
        st.write('Crea una figura (<code>fig</code>) y un conjunto de ejes (<code>ax</code>). <code>plt.subplots()</code> es una forma conveniente de crear una figura con un solo subplot.')
        
        st.markdown("<span style='color:green'>goleadoresAgrupados.plot(kind='pie', autopct='%1.1f%%', startangle=140, ax=ax)</span>", unsafe_allow_html=True)
        st.write('Dibuja un gráfico de pastel (pie chart) utilizando los datos de <code>goleadoresAgrupados</code>.')
        
        st.markdown("<span style='color:green'>ax.set_title('Distribución de jugadores por país')</span>", unsafe_allow_html=True)
        st.write('Establece el título del gráfico como "Distribución de jugadores por país".')
        
        st.markdown("<span style='color:green'>ax.set_ylabel('')</span>", unsafe_allow_html=True)
        st.write('Elimina la etiqueta del eje Y.')
        
        st.markdown("<span style='color:green'>st.pyplot(fig)</span>", unsafe_allow_html=True)
        st.write('Muestra la figura (<code>fig</code>) en una aplicación de Streamlit.')
    
    with col2:
        plt.figure(figsize=(6, 6)) 
        fig, ax = plt.subplots()
        goleadoresAgrupados.plot(kind='pie', autopct='%1.1f%%', startangle=140, ax=ax)
        ax.set_title('Distribución de jugadores por país')
        ax.set_ylabel('')
        st.pyplot(fig)
    
    # Sección 3: Selección de jugador
    st.title('También podemos seleccionar un jugador y ver sus datos')
    col3, col4 = st.columns(2)
    
    with col3:
        st.write('Creamos un cuadro donde el usuario puede seleccionar el goleador que prefiera:')
        st.markdown("<span style='color:green'>goleador = st.selectbox('Goleadores', goleadoresHist['Jugador'])</span>", unsafe_allow_html=True)
        
        st.write('Luego, por cada jugador seleccionado mostramos sus datos:')
        st.markdown("<span style='color:green'>if goleadoresHist['Jugador'].isin([goleador]).any():</span>", unsafe_allow_html=True)
        st.write("Esta línea de código verifica si el valor <code>goleador</code> está presente en la columna <code>Jugador</code> del DataFrame <code>goleadoresHist</code>. Si <code>goleador</code> se encuentra en la columna, la condición será verdadera y se ejecutará el bloque de código correspondiente.")
        
        st.write('Los datos mostrados por cada jugador se obtienen de la siguiente manera:')
        st.markdown("<span style='color:green'>goleadorSelec = goleadoresHist[goleadoresHist['Jugador'] == goleador].iloc[0]</span>", unsafe_allow_html=True)
        st.write('Esta línea de código filtra el DataFrame <code>goleadoresHist</code> para encontrar la fila donde el valor de la columna <code>Jugador</code> es igual a <code>goleador</code>, y luego selecciona la primera fila de este filtro. La fila seleccionada se asigna a la variable <code>goleadorSelec</code>.')
        
        st.write('Luego, con <code>st.info()</code> podemos mostrar los datos del jugador seleccionado, utilizando un fString:')
        st.markdown("<span style='color:green'>st.info(f'{goleador} debutó en {goleadorSelec['Debut']} con la selección {goleadorSelec['Selección']}\nAnotando {goleadorSelec['G']} goles en {goleadorSelec['PJ']} partidos\nSu promedio de goles por partido es de {goleadorSelec['Prom.']}%')</span>", unsafe_allow_html=True)
    
    with col4:
        goleador = st.selectbox('Goleadores', goleadoresHist['Jugador'])
        if goleadoresHist['Jugador'].isin([goleador]).any():
            goleadorSelec = goleadoresHist[goleadoresHist['Jugador'] == goleador].iloc[0]
            st.info(
                f'{goleador} debutó en {goleadorSelec["Debut"]} con la selección {goleadorSelec["Selección"]}\n'
                f'\n'
                f'Anotando {goleadorSelec["G"]} goles en {goleadorSelec["PJ"]} partidos\n'
                f'\n'
                f'Su promedio de goles por partido es de {goleadorSelec["Prom."]}%\n'
            )



def campeon():
     cuarto1=final[final['fecha']=='4-07-2024']
     cuarto2=final[final['fecha']=='5-07-2024']
     cuarto3=final[final['fecha']=='6-07-2024']
     cuarto4=final[final['fecha']=='6-07-2024 ']
     semi1=final[final['fecha']=='9-07-2024']
     semi2=final[final['fecha']=='10-07-2024']
     final1=final[final['fecha']=='14-07-2024']
     img_path = 'imagenes/Copa_America_2024_Logo.png'
     imag='imagenes/Argentina.png'
     with open(img_path, "rb") as imageFile:
         image_base64original = base64.b64encode(imageFile.read()).decode() 
     with open(imag,"rb") as imageFile:
         imagenSelect=base64.b64encode(imageFile.read()).decode() 
     img_pathC1 = (f"""imagenes/{cuarto1["equipo1"].iloc[0]}.png""")
     img_pathS1 = (f"""imagenes/{cuarto1["equipo2"].iloc[0]}.png""")
     with open(img_pathC1, "rb") as imageFile:
         image_base64C1 = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS1, "rb") as imageFile:
         image_base64S1 = base64.b64encode(imageFile.read()).decode()
     img_pathC2 = (f"""imagenes/{cuarto2["equipo1"].iloc[0]}.png""")
     img_pathS2 = (f"""imagenes/{cuarto2["equipo2"].iloc[0]}.png""")
     with open(img_pathC2, "rb") as imageFile:
         image_base64C2 = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS2, "rb") as imageFile:
         image_base64S2 = base64.b64encode(imageFile.read()).decode()
     img_pathC3 = (f"""imagenes/{cuarto3["equipo1"].iloc[0]}.png""")
     img_pathS3 = (f"""imagenes/{cuarto3["equipo2"].iloc[0]}.png""")
     with open(img_pathC3, "rb") as imageFile:
         image_base64C3 = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS3, "rb") as imageFile:
         image_base64S3 = base64.b64encode(imageFile.read()).decode()
     img_pathC4 = (f"""imagenes/{cuarto4["equipo1"].iloc[0]}.png""")
     img_pathS4 = (f"""imagenes/{cuarto4["equipo2"].iloc[0]}.png""")
     with open(img_pathC4, "rb") as imageFile:
         image_base64C4 = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS4, "rb") as imageFile:
         image_base64S4 = base64.b64encode(imageFile.read()).decode()
     img_pathC1s = (f"""imagenes/{semi1["equipo1"].iloc[0]}.png""")
     img_pathS1s = (f"""imagenes/{semi1["equipo2"].iloc[0]}.png""")
     with open(img_pathC1s, "rb") as imageFile:
         image_base64C1s = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS1s, "rb") as imageFile:
         image_base64S1s = base64.b64encode(imageFile.read()).decode()
     img_pathC2s = (f"""imagenes/{semi2["equipo1"].iloc[0]}.png""")
     img_pathS2s = (f"""imagenes/{semi2["equipo2"].iloc[0]}.png""")
     with open(img_pathC2s, "rb") as imageFile:
         image_base64C2s = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS2s, "rb") as imageFile:
         image_base64S2s = base64.b64encode(imageFile.read()).decode()
     img_pathC1f = (f"""imagenes/{final1["equipo1"].iloc[0]}.png""")
     img_pathS1f = (f"""imagenes/{final1["equipo2"].iloc[0]}.png""")
     with open(img_pathC1f, "rb") as imageFile:
         image_base64C1f = base64.b64encode(imageFile.read()).decode()
     with open(img_pathS1f, "rb") as imageFile:
         image_base64S1f = base64.b64encode(imageFile.read()).decode()
     html = """
     <style>
     .bracket-container {
         display: flex;
         justify-content: center;
         align-items: center;
         width: 100%;
         padding: 20px;
         background-color: #f0f0f0;
         border-radius: 10px;
         flex-wrap: nowrap;
     }
     .round {
         display: flex;
         flex-direction: column;
         justify-content: space-around;
         align-items: center;
         flex: 1;
         margin: 0 5px; /* Adjust margin for better alignment */
         max-width: 100%; /* Ensure container doesn't exceed available width */
     }
     .matchup {
         font-size: 12px;
         border: 1px solid #ccc;
         border-radius: 5px;
         padding: 10px;
         margin-bottom: 10px;
         width: 100%;
         text-align: center;
         background-color: #fff;
     }
     .final-matchup {
         font-size: 19px; /* Larger font size */
         border: 2px solid #ccc;
         border-radius: 5px;
         padding: 20px;
         margin-bottom: 20px;
         width: 80%; /* Larger width */
         text-align: center;
         background-color: #fff;
         position: relative;
     }
     .flag-container {
         position: fixed;
         top: 50%;
         left: 50%;
         transform: translate(-50%, -50%);
         text-align: center;
     }
     /* Media queries for smaller screen sizes */
     @media screen and (max-width: 768px) {
         
         .final-matchup {
             font-size: 12px; /* Adjust font size */
             padding: 15px; /* Adjust padding */
         }
     }
     </style>
     """
     
     html += f"""
     <div style="background-color:#f0f0f0; padding: 15px; border-radius: 15px; text-align: center; display: flex; justify-content: center; align-items: center; height: 100vh;">
         <div>
             <img src='data:image/png;base64,{image_base64original}' style='margin: auto; display: block; height: 250px;'>
             <div>&nbsp;&nbsp;</div>
         </div>
     </div>
     <div class="final-container" style="text-align: center; background-color:#f0f0f0;display: flex; justify-content:center;lign-items: center;">
         <div class="final-matchup">
             <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                 <img src='data:image/png;base64,{image_base64C1f}' style="width: 20px; height: auto; margin-right: 5px;">
                 <div style="flex: 1; text-align: left;">{final1["equipo1"].iloc[0]}</div>
                 <div>&nbsp;&nbsp;</div>
                 <div style="flex: 0 1 auto;">{final1["golesC"].iloc[0]}</div>
                 <div style="flex: 0 1 auto;"> - </div>
                 <div style="flex: 0 1 auto;">{final1["golesS"].iloc[0]}</div>
                 <div>&nbsp;&nbsp;</div>
                 <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                     <div style="flex: 1; text-align: right;">{final1["equipo2"].iloc[0]}</div>
                     <img src='data:image/png;base64,{image_base64S1f}' style="width: 20px; height: auto; margin-left: 5px;">
                 </div>
             </div>
             <div style="text-align: center; margin-top: 10px;">{final1["fecha"].iloc[0]}</div>
         </div>
     </div>
     <div class="bracket-container">
         <div class="round">
             <div class="matchup">
                 <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                     <img src='data:image/png;base64,{image_base64C1}' style="width: 20px; height: auto; margin-right: 5px;">
                     <div style="flex: 1; text-align: left;">{cuarto1["equipo1"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 0 1 auto;">{cuarto1["golesC"].iloc[0]}</div>
                     <div style="flex: 0 1 auto;"> - </div>
                     <div style="flex: 0 1 auto;">{cuarto1["golesS"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                         <div style="flex: 1; text-align: right;">{cuarto1["equipo2"].iloc[0]}</div>
                         <img src='data:image/png;base64,{image_base64S1}' style="width: 20px; height: auto; margin-left: 5px;">
                     </div>
                 </div>
                 <div style="text-align: center; margin-top: 10px;">{cuarto1["fecha"].iloc[0]}</div>
             </div>
             <div class="matchup">
                 <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                     <img src='data:image/png;base64,{image_base64C2}' style="width: 20px; height: auto; margin-right: 5px;">
                     <div style="flex: 1; text-align: left;">{cuarto2["equipo1"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 0 1 auto;">{cuarto2["golesC"].iloc[0]}</div>
                     <div style="flex: 0 1 auto;"> - </div>
                     <div style="flex: 0 1 auto;">{cuarto2["golesS"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                         <div style="flex: 1; text-align: right;">{cuarto2["equipo2"].iloc[0]}</div>
                         <img src='data:image/png;base64,{image_base64S2}' style="width: 20px; height: auto; margin-left: 5px;">
                     </div>
                 </div>
                 <div style="text-align: center; margin-top: 10px;">{cuarto2["fecha"].iloc[0]}</div>
             </div>
         </div>
         <div class="round">
             <div class="matchup">
                 <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                     <img src='data:image/png;base64,{image_base64C1s}' style="width: 20px; height: auto; margin-right: 5px;">
                     <div style="flex: 1; text-align: left;">{semi1["equipo1"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 0 1 auto;">{semi1["golesC"].iloc[0]}</div>
                     <div style="flex: 0 1 auto;"> - </div>
                     <div style="flex: 0 1 auto;">{semi1["golesS"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                         <div style="flex: 1; text-align: right;">{semi1["equipo2"].iloc[0]}</div>
                         <img src='data:image/png;base64,{image_base64S1s}' style="width: 20px; height: auto; margin-left: 5px;">
                     </div>
                 </div>
                 <div style="text-align: center; margin-top: 10px;">{semi1["fecha"].iloc[0]}</div>
             </div>
         </div>
         <div class="round">
             <div class="matchup">
                 <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                     <img src='data:image/png;base64,{image_base64C2s}' style="width: 20px; height: auto; margin-right: 5px;">
                     <div style="flex: 1; text-align: left;">{semi2["equipo1"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 0 1 auto;">{semi2["golesC"].iloc[0]}</div>
                     <div style="flex: 0 1 auto;"> - </div>
                     <div style="flex: 0 1 auto;">{semi2["golesS"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                         <div style="flex: 1; text-align: right;">{semi2["equipo2"].iloc[0]}</div>
                         <img src='data:image/png;base64,{image_base64S2s}' style="width: 20px; height: auto; margin-left: 5px;">
                     </div>
                 </div>
                 <div style="text-align: center; margin-top: 10px;">{semi2["fecha"].iloc[0]}</div>
             </div>
         </div>
         <div class="round">
             <div class="matchup">
                 <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                     <img src='data:image/png;base64,{image_base64C3}' style="width: 20px; height: auto; margin-right: 5px;">
                     <div style="flex: 1; text-align: left;">{cuarto3["equipo1"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 0 1 auto;">{cuarto3["golesC"].iloc[0]}</div>
                     <div style="flex: 0 1 auto;"> - </div>
                     <div style="flex: 0 1 auto;">{cuarto3["golesS"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                         <div style="flex: 1; text-align: right;">{cuarto3["equipo2"].iloc[0]}</div>
                         <img src='data:image/png;base64,{image_base64S3}' style="width: 20px; height: auto; margin-left: 5px;">
                     </div>
                 </div>
                 <div style="text-align: center; margin-top: 10px;">{cuarto3["fecha"].iloc[0]}</div>
             </div>
             <div class="matchup">
                 <div style="display: flex; justify-content: space-between; margin-top: 10px;">
                     <img src='data:image/png;base64,{image_base64C4}' style="width: 20px; height: auto; margin-right: 5px;">
                     <div style="flex: 1; text-align: left;">{cuarto4["equipo1"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 0 1 auto;">{cuarto4["golesC"].iloc[0]}</div>
                     <div style="flex: 0 1 auto;"> - </div>
                     <div style="flex: 0 1 auto;">{cuarto4["golesS"].iloc[0]}</div>
                     <div>&nbsp;&nbsp;</div>
                     <div style="flex: 1; display: flex; align-items: center; justify-content: flex-end;">
                         <div style="flex: 1; text-align: right;">{cuarto4["equipo2"].iloc[0]}</div>
                         <img src='data:image/png;base64,{image_base64S4}' style="width: 20px; height: auto; margin-left: 5px;">
                     </div>
                 </div>
                 <div style="text-align: center; margin-top: 10px;">{cuarto4["fecha"].iloc[0]}</div>
             </div>
         </div>
     </div>

     """

    
 
     st.markdown(html, unsafe_allow_html=True)
         
  
    
def campeon2024():
     img_path = 'imagenes/Argentina.png'
     
     # Verificar que la imagen se carga correctamente
     try:
         with open(img_path, "rb") as imageFile:
             image_base64original = base64.b64encode(imageFile.read()).decode()
     except FileNotFoundError:
         st.error("No se encontró la imagen en la ruta especificada.")
         return
 
     
      # HTML y CSS para la bandera flotante centrada
     flag_html = """
     <style>
     @keyframes move {
         0% { top: 50%; transform: translateY(-50%); }
         50% { top: calc(100vh - 50px); transform: translateY(-50%); } /* Ajusta 50px según el tamaño de tu imagen */
         100% { top: 50%; transform: translateY(-50%); }
     }
     
     .flag-container {
         position: fixed;
         left: 50%;
         transform: translateX(-50%);
         animation: move 5s infinite;
         text-align: center;
     }
     </style>
     """
 
     flag_html += f"""
     <div class="flag-container" id="flag">
         <img src='data:image/png;base64,{image_base64original}' alt="Argentina" width="50px">
         <div style="text-align: center"><strong>Bicampeón de la Copa América!</strong></div>
     </div>
     """
 
     st.markdown(flag_html, unsafe_allow_html=True)
 
            
def campConsecutivos():
     st.title("Los campeones consecutivos son: Argentina, Uruguay, Brasil y Chile")
     st.markdown("Veamos los Tricampeonatos y Bicampeonatos de cada Selección")
     
     # Datos de campeonatos consecutivos
     campeones_consecutivos = pd.DataFrame({
         "Seleccion": ["Argentina", "Uruguay", "Brasil", "Chile"],
         "Tricampeonatos": [1, 0, 0, 0],
         "Bicampeonatos": [3, 3, 2, 1],
         "FechaT": ["1945, 1946, 1947", "", "", ""],
         "FechaB": ["1957, 1959; 1991, 1993; 2021 , 2024", "1916, 1917; 1923, 1924; 1956, 1959", "1997, 1999; 2004, 2007", "2015, 2016"]
     })
     
     # Datos históricos de la Copa América
     copa_america_historico = pd.DataFrame({
         "Anio": [1945, 1946, 1947, 1957, 1959, 1991, 1993,2021,2024, 1916, 1917, 1923, 1924, 1935, 1956, 1959, 1997, 1999, 2004, 2007, 2019, 2015, 2016],
         "Seleccion": ["Argentina", "Argentina", "Argentina", "Argentina", "Argentina","Argetina","Argetina", "Argentina", "Argentina", "Uruguay", "Uruguay", "Uruguay", "Uruguay", "Uruguay", "Uruguay", "Uruguay", "Brasil", "Brasil", "Brasil", "Brasil", "Brasil", "Chile", "Chile"],
         "Subcampeon": ["Chile", "Brasil", "Paraguay", "Brasil", "Chile", "Brasil", "México", "Chile", "Argentina", "Argentina", "Paraguay", "Argentina", "Chile", "Argentina", "Bolivia", "Uruguay", "Argentina", "México", "Perú", "Perú", "Argentina", "Argentina","Colombia"]
     })
     
     # Gráfico para cada selección
     for index, row in campeones_consecutivos.iterrows():
         st.subheader(f"{row['Seleccion']}:")
         
         # Crear figura
         fig = go.Figure()
         
         # Agregar tricampeonatos si existen
         if row["Tricampeonatos"] > 0:
             tricampeonatos = list(map(int, row["FechaT"].split(', ')))
             fig.add_trace(go.Scatter(x=tricampeonatos, y=[3] * len(tricampeonatos), mode='lines+markers', name='Tricampeonatos'))
         
         # Agregar bicampeonatos si existen
         if row["Bicampeonatos"] > 0:
             for bicampeonatos in row["FechaB"].split('; '):
                 años = list(map(int, bicampeonatos.split(', ')))
                 fig.add_trace(go.Scatter(x=años, y=[2] * len(años), mode='lines+markers', name='Bicampeonatos'))
         
         # Configuración del gráfico
         fig.update_layout(title=f"Logros de {row['Seleccion']} en la Copa América",
                           xaxis_title="Años",
                           yaxis_title="Tipo de Campeonato",
                           yaxis=dict(tickvals=[2, 3], ticktext=['Bicampeonatos', 'Tricampeonatos']),
                           showlegend=True)
         
         st.plotly_chart(fig)
         
         # Agregar detalles debajo del gráfico
         if row["Tricampeonatos"] > 0:
             st.write(f"<h3>{row['Seleccion']} ha ganado {row['Tricampeonatos']} tricampeonato(s) en las fechas {row['FechaT']}:</h3>", unsafe_allow_html=True)
             for anio in tricampeonatos:
                 subcampeon = copa_america_historico[copa_america_historico['Anio'] == anio]['Subcampeon'].values[0]
                 st.write(f"- En {anio} se impuso a {subcampeon}.")
             st.markdown('<h5>En conclusión, Argentina es el único país latinoamericano que ganó un tricampeonato 🏆</h5>', unsafe_allow_html=True)
         else:
             st.write(f"{row['Seleccion']} no ha ganado tricampeonatos.")
         
         if row["Bicampeonatos"] > 0:
             st.write(f"<h3>{row['Seleccion']} ha ganado {row['Bicampeonatos']} bicampeonato(s) en las fechas {row['FechaB']}:</h3>", unsafe_allow_html=True)
             for bicampeonatos in row["FechaB"].split('; '):
                 años = list(map(int, bicampeonatos.split(', ')))
                 for anio in años:
                     subcampeon = copa_america_historico[copa_america_historico['Anio'] == anio]['Subcampeon'].values[0]
                     st.write(f"- En {anio} se impuso a {subcampeon}.")
         else:
             st.write(f"{row['Seleccion']} no ha ganado bicampeonatos.")

def main():     
     if tipo_seccion == 'Campeones consecutivos':
         campConsecutivos()
     elif tipo_seccion =='Campeon 2024':
         st.write("""<h3 style="text-align: center;">Resultado de la final</h3>""",unsafe_allow_html=True)
         campeon()
         campeon2024()
     elif tipo_seccion =='Fase de grupo y sedes':
         estadiosYFaseDeGrupos()
         st.write("Mapa de ubicaciones de los partidos:")
         mapa = crear_mapa()
         folium_static(mapa)
         fechas()
     elif tipo_seccion == rendimiento:
         datosJugadoresLista = []
         jugSeleccion = datosJugadores[datosJugadores['equipo'] == tipo_seccion]
         for index, row in jugSeleccion.iterrows():
             datosJugadoresLista.append(row)
         randomJ = random.choice(datosJugadoresLista)
         rend(tipo_seccion)
         adivinador(tipo_seccion, randomJ)
     elif tipo_seccion == 'Goleadores históricos':
         estadisticaGoleadores()
     else:
         print('')



if __name__=="__main__":
     main()
      