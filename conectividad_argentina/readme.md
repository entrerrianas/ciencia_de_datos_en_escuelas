# Clusters en mapas de streamlit

En el archivo [mapas_cluster.ipynb](https://gitlab.com/entrerrianas/ciencia_de_datos_en_escuelas/-/blob/main/conectividad_argentina/mapas_cluster.ipynb?ref_type=heads) se encuentra un ejemplo para agrupar puntos de un dataset en cluster de manera dinámica según el zoom.